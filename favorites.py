first_question = "What is your favorite color?"
first_question_answer = "Black"

second_question = "What is your favorite food?"
second_question_answer = "Fried Rice"

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = "Tom and Jerry"

fourth_question = "What is your favorite animal?"
fourth_question_answer = "Cats"

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = "PowerShell"
